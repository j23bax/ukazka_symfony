<?php

namespace App\Repository;

use App\Entity\Client;
use App\Exception\ClientException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /**
     * @param array $data
     * @return Client|null
     * @throws ClientException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function insert(array $data): ?Client
    {
        $client = new Client();

        if (!isset($data["firstName"]) || !isset($data["lastName"]) || !isset($data["phoneNumber"]) || !isset($data["mailAddress"])) {
            throw new ClientException("Data corrupted!");
        }

        $client->setFirstName($data["firstName"])
            ->setLastName($data["lastName"])
            ->setPhoneNumber($data["phoneNumber"])
            ->setMailAddress($data["mailAddress"]);

        $this->getEntityManager()->persist($client);
        $this->getEntityManager()->flush();

        return $client;
    }

    /**
     * @param array $data
     * @param $id
     * @return Client|null
     * @throws ClientException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function update(array $data, $id): ?Client
    {
        /** @var Client $client */
        $client = $this->getSingleById($id);

        if (!isset($data["firstName"]) || !isset($data["lastName"]) || !isset($data["phoneNumber"]) || !isset($data["mailAddress"])) {
            throw new ClientException("Data corrupted!");
        }

        $client->setFirstName($data["firstName"])
            ->setLastName($data["lastName"])
            ->setPhoneNumber($data["phoneNumber"])
            ->setMailAddress($data["mailAddress"]);

        $this->getEntityManager()->persist($client);
        $this->getEntityManager()->flush();

        return $client;
    }

    /**
     * @param $id
     * @param bool $getQuery
     * @return QueryBuilder|mixed|null
     * @throws \Exception
     */
    public function getSingleById($id, bool $getQuery = false)
    {
        $qb = $this->createQueryBuilder('u');

        $query = $qb->where("u.id = :id")->setParameter("id", $id);

        if ($getQuery) {
            return $query;
        }

        try {
            $result = $query->getQuery()->getSingleResult();
        } catch (NoResultException $ex) {
            return NULL;
        } catch (\Exception $ex) {
            throw $ex;
        }

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getFormDataById($id)
    {
        /** @var QueryBuilder $singleQB */
        $singleQB = $this->getSingleById($id, true);
        $result = $singleQB->getQuery()->getResult(Query::HYDRATE_ARRAY)[0];

        return $result;
    }

    /**
     * @param $entity
     * @return bool
     * @throws \Exception
     */
    public function remove($entity)
    {
        try {
            $this->getEntityManager()->remove($entity);
            $this->getEntityManager()->flush($entity);
        } catch (\Exception $exception) {
            throw $exception;
        }

        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function removeById($id)
    {
        return $this->remove($this->getSingleById($id));
    }
}
